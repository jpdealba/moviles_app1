import 'package:flutter/material.dart';

class MainPage extends StatefulWidget {
  MyApp createState() => MyApp();
}

void main() => runApp(MainPage());

SnackBar snack(String message) {
  return new SnackBar(
      content: Text(message.toString()),
      action: SnackBarAction(
        label: 'Undo',
        onPressed: () {},
      ));
}

class MyApp extends State<MainPage> {
  int itemCount = 0;
  var likeButtonColor = Colors.grey;
  var dislikeButtonColor = Colors.grey;
  var mailButton = Colors.black;
  var phoneButton = Colors.black;
  var directionButton = Colors.black;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: Text('app ITESO'),
        ),
        body: SingleChildScrollView(
            child: Builder(
                builder: (context) => Column(
                      children: [
                        Image.network(
                            "https://cruce.iteso.mx/wp-content/uploads/sites/123/2018/04/Portada-2-e1525031912445.jpg"),
                        ListTile(
                            title: Text(
                                "El ITESO Universidad Jesuita de Guadalajara",
                                style: new TextStyle(
                                    fontSize: 13, fontWeight: FontWeight.bold)),
                            subtitle: Text("San Pedro Tlaquepaque, Jal.",
                                style: new TextStyle(fontSize: 12)),
                            trailing: SizedBox(
                                width: 130,
                                child: Row(
                                  children: [
                                    IconButton(
                                      icon: Icon(Icons.thumb_up),
                                      color: likeButtonColor,
                                      onPressed: () => setState(() => {
                                            itemCount++,
                                            likeButtonColor == Colors.grey
                                                ? likeButtonColor = Colors.blue
                                                : likeButtonColor = Colors.grey
                                          }),
                                    ),
                                    IconButton(
                                      icon: Icon(Icons.thumb_down),
                                      color: dislikeButtonColor,
                                      onPressed: () => setState(() => {
                                            itemCount--,
                                            dislikeButtonColor == Colors.grey
                                                ? dislikeButtonColor =
                                                    Colors.blue
                                                : dislikeButtonColor =
                                                    Colors.grey
                                          }),
                                    ),
                                    Flexible(
                                        child: new Text(itemCount.toString()))
                                  ],
                                ))),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Column(
                              children: [
                                IconButton(
                                    color: mailButton,
                                    icon: Icon(
                                      Icons.mail,
                                      size: 35,
                                    ),
                                    onPressed: () {
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(snack("Enviar Correo"));
                                      setState(() => {
                                            mailButton == Colors.black
                                                ? mailButton = Colors.blue
                                                : mailButton = Colors.black
                                          });
                                    }),
                                Text("Correo")
                              ],
                            ),
                            Column(
                              children: [
                                IconButton(
                                    color: phoneButton,
                                    icon: Icon(
                                      Icons.phone,
                                      size: 35,
                                    ),
                                    onPressed: () {
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(snack("Hacer llamada"));
                                      setState(() => {
                                            phoneButton == Colors.black
                                                ? phoneButton = Colors.blue
                                                : phoneButton = Colors.black
                                          });
                                    }),
                                Text("Llamada")
                              ],
                            ),
                            Column(
                              children: [
                                IconButton(
                                    color: directionButton,
                                    icon: Icon(
                                      Icons.directions,
                                      size: 35,
                                    ),
                                    onPressed: () {
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(snack("Ir al ITESO"));
                                      setState(() => {
                                            directionButton == Colors.black
                                                ? directionButton = Colors.blue
                                                : directionButton = Colors.black
                                          });
                                    }),
                                Text("Ruta")
                              ],
                            ),
                          ],
                        ),
                        Container(
                          margin: const EdgeInsets.only(
                              left: 15.0, right: 15.0, top: 40),
                          child: Text(
                            "El ITESO, Universidad Jesuita de Guadalajara (Instituto Tecnológico y de Estudios Superiores de Occidente), es una universidad privada ubicada en la Zona Metropolitana de Guadalajara, Jalisco, México, fundada en el año 1957. La institución forma parte del Sistema Universitario Jesuita (SUJ) que integra a ocho universidades en México. La universidad es nombrada como la Universidad Jesuita de Guadalajara El ITESO, Universidad Jesuita de Guadalajara (Instituto Tecnológico y de Estudios Superiores de Occidente), es una universidad privada ubicada en la Zona Metropolitana de Guadalajara, Jalisco, México, fundada en el año 1957. La institución forma parte del Sistema Universitario Jesuita (SUJ) que integra a ocho universidades en México. La universidad es nombrada como la Universidad Jesuita de Guadalajara El ITESO, Universidad Jesuita de Guadalajara (Instituto Tecnológico y de Estudios Superiores de Occidente), es una universidad privada ubicada en la Zona Metropolitana de Guadalajara, Jalisco, México, fundada en el año 1957. La institución forma parte del Sistema Universitario Jesuita (SUJ) que integra a ocho universidades en México. La universidad es nombrada como la Universidad Jesuita de Guadalajara",
                            style: new TextStyle(fontWeight: FontWeight.w600),
                          ),
                        )
                      ],
                    ))),
      ),
    );
  }
}
